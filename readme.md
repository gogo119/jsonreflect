# JsonReflect介绍
##### `JsonReflect`是基于C++11实现的跨平台JSON序列化与反序列化库。支持将对象序列化为JSON字符串或者将JSON字符串反序列化为对象。作者利用宏与C++11的`typeid`运算符实现一种动态反射机制，大大简化了序列化与反序列化工作。


# 安装方法
#### 1、下载源码

git clone https://gitee.com/xungen/jsonreflect.git

#### 2、在源码目录下执行`make`命令编译输出静态库文件`libjson.a`

#### 3、在源码目录下执行`make test`命令可以查看`JsonReflect`库测试结果

#### 4、源码目前下有一个名为`test.cpp`的示例代码，你可以参考示例代码的使用方法，示例代码内容如下：

```
#include "json.h"

JsonEntity(Item)
{
public:
	//定义名为ival的int类型成员
	rint(ival);
	//定义名为bval的bool类型成员
	rbool(bval);
	//定义名为sval的string类型成员
	rstring(sval);
};

JsonEntity(Entity)
{
public:
	//定义名为ival的int类型成员
	rint(ival);
	//定义名为bval的bool类型成员
	rbool(bval);
	//定义名为sval的string类型成员
	rstring(sval);
	//定义名为list的vector<Item>类型成员
	rarray(Item, list);
};

int main(int argc, char** argv)
{
	Entity obj;
	Entity tmp;
	sp<Item> item;
	
	obj.ival = 0;
	obj.bval = false;
	obj.sval = "zero";

	item = obj.list.add();
	item->ival = 1;
	item->bval = true;
	item->sval = "one";

	item = obj.list.add();
	item->ival = 2;
	item->bval = true;
	item->sval = "two";

	item = obj.list.add();
	item->ival = 3;
	item->bval = true;
	item->sval = "three";

	//对象序列化为JSON字符串
	cout << obj.toString() << endl << endl;

	cout << endl;

	//JSON字符串反序列化为对象
	tmp.fromString(obj.toString());

	cout << tmp.toString() << endl << endl;

	return 0;
}
```